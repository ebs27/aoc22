﻿<?xml version='1.0' encoding='UTF-8'?>
<Project Type="Project" LVVersion="22308000">
	<Property Name="NI.LV.All.SourceOnly" Type="Bool">true</Property>
	<Item Name="My Computer" Type="My Computer">
		<Property Name="server.app.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.control.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.tcp.enabled" Type="Bool">false</Property>
		<Property Name="server.tcp.port" Type="Int">0</Property>
		<Property Name="server.tcp.serviceName" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.tcp.serviceName.default" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.vi.callsEnabled" Type="Bool">true</Property>
		<Property Name="server.vi.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="specify.custom.address" Type="Bool">false</Property>
		<Item Name="Day1" Type="Folder">
			<Item Name="Day1Part1.vi" Type="VI" URL="../Day1/Day1Part1.vi"/>
			<Item Name="Day1Part2.vi" Type="VI" URL="../Day1/Day1Part2.vi"/>
		</Item>
		<Item Name="Day2" Type="Folder">
			<Item Name="Day2.vi" Type="VI" URL="../Day2/Day2.vi"/>
		</Item>
		<Item Name="Day3" Type="Folder">
			<Item Name="checkmatches--rev1.vi" Type="VI" URL="../Day3/checkmatches--rev1.vi"/>
			<Item Name="Day3.vi" Type="VI" URL="../Day3/Day3.vi"/>
			<Item Name="Day3Part2.vi" Type="VI" URL="../Day3/Day3Part2.vi"/>
		</Item>
		<Item Name="Testers" Type="Folder">
			<Item Name="Day1Part2Tester.vi" Type="VI" URL="../Tester/Day1Part2Tester.vi"/>
			<Item Name="Tester.vi" Type="VI" URL="../Tester/Tester.vi"/>
		</Item>
		<Item Name="Dependencies" Type="Dependencies">
			<Item Name="vi.lib" Type="Folder">
				<Item Name="Less Comparable.lvclass" Type="LVClass" URL="/&lt;vilib&gt;/Comparison/Less/Less Comparable/Less Comparable.lvclass"/>
				<Item Name="Less Functor.lvclass" Type="LVClass" URL="/&lt;vilib&gt;/Comparison/Less/Less Functor/Less Functor.lvclass"/>
				<Item Name="Less.vim" Type="VI" URL="/&lt;vilib&gt;/Comparison/Less.vim"/>
				<Item Name="Sort 1D Array Core.vim" Type="VI" URL="/&lt;vilib&gt;/Array/Helpers/Sort 1D Array Core.vim"/>
				<Item Name="Sort 1D Array.vim" Type="VI" URL="/&lt;vilib&gt;/Array/Sort 1D Array.vim"/>
			</Item>
		</Item>
		<Item Name="Build Specifications" Type="Build"/>
	</Item>
</Project>
